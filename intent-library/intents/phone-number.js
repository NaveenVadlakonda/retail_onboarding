"use strict";

/**
 * phone number validation controller
 * @param {object} df webhook fulfillment object
 */

const firestore = require('../.././helper/firestore');
const db = firestore.db;
let flag;
 const phoneNumberValidationIntent = async (df) => {
  try{
    const phoneNumber = df._request.queryResult.parameters.phonenumber;
    let phoneNumberString = phoneNumber.toString().replace(/\s+/g, '');
    if(phoneNumberString.length == 10) {
        const snapshot = await db.collection('users').get();
        snapshot.forEach((doc) => {
          if(doc.data().phoneno == phoneNumber){
              flag = true;
              df.setResponseText("Great! can you please give your zipcode");
              df.setOutputContext("address",1,{});
          }
        });
        if(!flag) {
          df.setResponseText("I'm sorry! Your phone number is not registered with us!");
          df.setOutputContext("phone-number", 5, {});
        }
    }    
    else {
        df.setResponseText("Please enter a valid 10 digit phone number!");
        df.setOutputContext("phone-number", 5, {});
    }
  }
  catch(err){
    console.log(err);
  }
 } 

module.exports = phoneNumberValidationIntent;
