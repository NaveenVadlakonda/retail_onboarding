"use strict";

/**
 * account number validation controller
 * @param {object} df webhook fulfillment object
 */

const firestoreAuth = require('../.././helper/firestore');
const db = firestoreAuth.db;
const zipcodeValidationIntent = async (df) => {
try {
  const zipcode = df._request.queryResult.parameters.zipcode;
  let zipcodeString = zipcode.toString().replace(/\s+/g, '');
  const phoneNumber = df.getContext("phone-number").parameters.phonenumber;
  if(zipcodeString.length == 4) {
    const snapshot = await db.collection('users').get();
    snapshot.forEach((doc) => {
      if(doc.data().phoneno == phoneNumber){
          if(doc.data().zipcode == zipcode){
            df.setResponseText("Your Total Balance is "+doc.data().balance+". The minimum payment due is "+doc.data().amountdue+". Do you want to pay with the account ending in "+doc.data().cardno);
          }
        else{
            df.setResponseText("Please enter your registered zipcode number!");
            df.setOutputContext("address",3,{});
          }
      }
    });  
  }
  else {
      df.setResponseText("Please enter last 4 digits of your zipcode number!");
      df.setOutputContext("address",3,{});
  }
} 
catch(err){
  console.log(err);
}
}


module.exports = zipcodeValidationIntent;
